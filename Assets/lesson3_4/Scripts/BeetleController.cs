﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeetleController : MonoBehaviour
{
    public float movingSpeed = 1.0f;


    SpriteRenderer spriteRenderer;
    public Transform beetleDetectionTransform;
    Rigidbody2D newRigidbody;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        newRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //RaycastHit2D raycast = Physics2D.Raycast()
        //Move();
    }

    void FixedUpdate()
    {
        Move();
    }

    void Move()
    {
        newRigidbody.transform.Translate(Vector2.left * movingSpeed * Time.deltaTime);
        //newRigidbody.velocity = new Vector2(Vector2.left.x * movingSpeed, newRigidbody.velocity.y);
        BeetleDetect();
    }
    bool movingRight = false;
    void BeetleDetect()
    {
        RaycastHit2D raycast = Physics2D.Raycast(beetleDetectionTransform.position, Vector2.down, 0.5f );
        if (raycast.collider == false)
        {
            if (movingRight == true)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingRight = false;
            } else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingRight = true;
            }
        }
    }



}
