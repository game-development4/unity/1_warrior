﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthController : MonoBehaviour
{
    public static int lifeValue = 4;
    public float maxValue = 20f;
    public float healthValue = 20f;
    public Slider slider;
    public Text hpText;
    public Text lifeText;

    public void increaseHealth(float value)
    {
        healthValue += value;
        healthValue = healthValue >= maxValue ? maxValue : healthValue;
    }

    public void decreaseHealth(float value)
    {
        healthValue -= value;
    }

    public bool isAlive()
    {
        return healthValue > 0;
    }

    public bool isDeath()
    {
        return healthValue <= 0f;
    }

    public void die()
    {
        if (lifeValue > 0)
        {
            lifeValue--;
        }
        healthValue = 0;

    }

    IEnumerator Delay()
    {

        yield return new WaitForSeconds(3f);
        

    }

    public void fullHealth()
    {
        healthValue = slider.maxValue;
    }

    void Start()
    {
        if (PlayerHealthController.lifeValue <= 0)
        {
            PlayerHealthController.lifeValue = 2;
            PlayerScoreController.score = 0;
            PlayerController.level.Clear();
        }
    }

    // Update is called once per frame
    void Update()
    {
        slider.maxValue = maxValue;
        slider.value = healthValue;
        hpText.text = string.Format("x{0} HP", healthValue);
        lifeText.text = string.Format("x{0}", lifeValue);
    }
}
