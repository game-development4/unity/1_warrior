﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeadPointContoller : MonoBehaviour
{

    public Transform startPointTransform;
    public GameObject deadEffect, respawnEffect;
    public float waitForDie = 2f;
    public float waitForRespawn = 2f;

    PlayerHealthController playerHealthController;
    PlayerController playerController;
    GameObject playerObject;

    //Collision2D collision;

    bool only1EffectInstance = true;

    void Start()
    {
        playerHealthController = FindObjectOfType<PlayerHealthController>();
        playerController = FindObjectOfType<PlayerController>();
        playerObject = playerController.gameObject;
    }

    void Update()
    {
        if (playerHealthController.isDeath() && only1EffectInstance)
        {
            StartCoroutine(DieAndRespawnEffect());
            only1EffectInstance = false;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
 
        if (collision.collider.tag == "Player")
        {
            //StartCoroutine(DieAndRespawnEffect(collision));
            //this.collision = collision;
            playerHealthController.die();
        }
    }



    IEnumerator DieAndRespawnEffect()
    {
        Instantiate(deadEffect, playerObject.transform.position, playerObject.transform.rotation);
        yield return RespawnEffect();

    }

    public IEnumerator RespawnEffect()
    {
        playerController.dieSound.Play();
        playerObject.SetActive(false);
        yield return new WaitForSeconds(waitForDie);

        if (PlayerHealthController.lifeValue == 0)
        {
            SceneManager.LoadScene("GameOver", LoadSceneMode.Additive);
        }
        else
        {
            playerHealthController.fullHealth();
            only1EffectInstance = true;
            Instantiate(respawnEffect, startPointTransform.position, startPointTransform.rotation);
            playerObject.transform.position = startPointTransform.position;
            playerObject.transform.localScale = new Vector3(Mathf.Abs(playerObject.transform.localScale.x), playerObject.transform.localScale.y, playerObject.transform.localScale.z);
            playerController.respawnSound.Play();
            yield return new WaitForSeconds(waitForRespawn);
            playerController.respawnSound.Play();
            playerObject.SetActive(true);
            StartCoroutine(playerController.BlinkingShield());
        }


    }
}
