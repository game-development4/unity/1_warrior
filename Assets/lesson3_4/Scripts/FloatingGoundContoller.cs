﻿using System.Collections;
using UnityEngine;

public class FloatingGoundContoller : MonoBehaviour
{

    public float flyingSpeed = 1f;


    PlayerController player;
    bool isFlying = false;
    bool playerExit = false;
    

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {            
            playerExit = false;
            if(!isFlying)
                StartCoroutine(startFlying());
        }

    }

    void OnCollisionExit2D (Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            playerExit = true;
        }
    }

    IEnumerator startFlying()
    {
        yield return new WaitForSeconds(1);
        isFlying = true;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (player.playerHealthController.isDeath())
        {
            isFlying = false;
            transform.position = new Vector3(147f, 1.7f, 0f);
        }

        if (isFlying)
        {
            transform.Translate(Vector3.right * flyingSpeed * Time.deltaTime);
            if(!playerExit)
                player.transform.Translate(Vector3.right * flyingSpeed * Time.deltaTime);
        }

    }
}
