﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScoreController : MonoBehaviour
{
    public static int score = 0;
    public static int targetLifeScore = 500;
    public Text scoreText;

    PlayerController playerController;

    // Start is called before the first frame update
    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
    }

    public void increaseScore(int score)
    {
        PlayerScoreController.score += score;     
        if (PlayerScoreController.score >= targetLifeScore)
        {
            playerController.increaseLifeSound.Play();
            PlayerHealthController.lifeValue++;
            PlayerScoreController.targetLifeScore += 500;

        }
    }

    public IEnumerator endLevelInscreaseScore(AudioSource scoreCount)
    {
        yield return new WaitForSeconds(7f);

        playerController.scoreCount.Play();
        for (int i = 1; i <= 50; i++)
        {
            yield return new WaitForSeconds(0.05f);
            increaseScore(1);
        }
        playerController.scoreCount.Stop();
        yield return new WaitForSeconds(1f);        
        PlayerController.level.Add(Int16.Parse(SceneManager.GetActiveScene().name.Substring(SceneManager.GetActiveScene().name.Length - 1)));
        if (PlayerController.level.Count == 4)
        {
            SceneManager.LoadScene("Congratulations");
        } else
        {
            SceneManager.LoadScene("GameList");
        }
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = string.Format("Score: {0:D5}", score);
    }
}
