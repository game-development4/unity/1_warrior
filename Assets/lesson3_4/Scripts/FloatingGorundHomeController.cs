﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingGorundHomeController : MonoBehaviour
{

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name.Equals("FlyingGround", System.StringComparison.OrdinalIgnoreCase))
        {
            FindObjectOfType<FloatingGoundContoller>().enabled = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (FindObjectOfType<PlayerHealthController>().isDeath())
        {
            FindObjectOfType<FloatingGoundContoller>().enabled = true;
        }
    }
}
