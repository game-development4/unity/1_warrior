﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour
{

    public float offsetSpeed = 0.05f;

    // Start is called before the first frame update
    void Start()
    {
    }


    // Update is called once per frame
    void Update()
    {
        Vector2 offset = new Vector2(Time.time * offsetSpeed, 0);
        GetComponent<Renderer>().material.mainTextureOffset = offset;
    }
}
