﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject player;
    public float offsetY = 0f;
    public float offsetX = 0f;
    Transform ts;

    // Start is called before the first frame update
    void Start()
    {
        ts = GetComponent<Transform>();
    }

    void FollowCamera()
    {
        //Debug.Log(ts.position.x);
        ts.position = new Vector3(player.transform.position.x + offsetX, player.transform.position.y + offsetY, ts.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        FollowCamera();    
    }
}
