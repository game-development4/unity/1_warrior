﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class JoyStickController : MonoBehaviour
{

    public GameObject joyStickCanvas;
    public static bool d;
    public static bool a;
    public static bool k;
    public void DDown()
    {
        d = true;
    }

    public void DUp()
    {
        d = false;
    }

    public void ADown()
    {
        a = true;
    }

    public void AUp()
    {
        a = false;
    }

    public void KDown()
    {
        k = true;
    }

    public void KUp()
    {
        k = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!Application.isMobilePlatform)
        {
            joyStickCanvas.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
