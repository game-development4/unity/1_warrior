﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{

    public void Start()
    {
        if (SceneManager.GetActiveScene().name.Equals("GameList", System.StringComparison.OrdinalIgnoreCase))
            foreach (int i in PlayerController.level)
            {
                transform.GetChild(i-1).GetChild(0).GetComponent<Text>().color = Color.green;
            }
    }

    public void ClickPlay()
    {
        SceneManager.LoadScene("Level1");
    }

    public void ClickLevel(string level)
    {
        SceneManager.LoadScene("Level" + level);

        
    }
    
    public void Restart()
    {
        SceneManager.LoadScene("Menu");
    }

    public void ClickExit()
    {
        Application.Quit();
    }
}
