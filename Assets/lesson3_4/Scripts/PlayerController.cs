﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class PlayerController : MonoBehaviour
{

    public static string HORIZONTAL = "Horizontal";
    public static List<Int16> level = new List<Int16>();
    //public static int level = 0;

    Rigidbody2D rb;
    BoxCollider2D bc;
    Animator anim;

    public LayerMask Ground;

    public bool isWalking;
    public bool isAttacking;
    public bool isInjuring;
    public bool isJumping;
    public float injurdDuration = 3.0f;
    public float movingSpeed = 1.0f;
    public float jumpHeight = 0.0f;
    

    public AudioSource jumpSound;
    public AudioSource eatSound;
    public AudioSource hurtSound;
    public AudioSource dieSound;
    public AudioSource respawnSound;
    public AudioSource backgroundSound;
    public AudioSource winSound;
    public AudioSource scoreCount;
    public AudioSource increaseLifeSound;

    public GameObject completeLevelText;

    Renderer newRenderer;
    Color color;

    public PlayerHealthController playerHealthController;
    PlayerScoreController playerScoreController;
    JoyStickController joyStickController;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "EndHome")
        {
            anim.SetBool("isJumping", false);
            isJumping = false;
            enabled = false;
            backgroundSound.Stop();
            winSound.Play();
            completeLevelText.SetActive(true);
            StartCoroutine(playerScoreController.endLevelInscreaseScore(scoreCount));
        }
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.tag == "Food")
        {
            //Debug.Log(collider.tag);
            eatSound.Play();
            Destroy(collider.gameObject);
            playerHealthController.increaseHealth(2f);
            playerScoreController.increaseScore(MushroomController.mushroomScore);
        } else if (collider.tag == "Enemy" && !isInjuring)
        {
            hurtSound.Play();
            StartCoroutine(BlinkingShield());

            switch (collider.name)
            {
                case string a when a.Contains("Beetle"):
                        playerHealthController.decreaseHealth(4f);
                    break;

            }
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {

        newRenderer = GetComponent<Renderer>();
        color = newRenderer.material.color;
        rb = transform.GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        bc = transform.GetComponent<BoxCollider2D>();
        playerHealthController = FindObjectOfType<PlayerHealthController>();
        playerScoreController = FindObjectOfType<PlayerScoreController>();
        joyStickController = FindObjectOfType<JoyStickController>();

        //jumpSound = Resources.Load<AudioClip>("Audio/Jump.wav");
        //audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    //void FixedUpdate()
    //{
    //    //Movement();
    //    //isGrounded = Physics2D.OverlapBox(GroundChecked.transform.position, new Vector2(1, 1), 0f, Ground);
    //    isGrounded = 
    //    Debug.Log(isGrounded);
    //}

    public IEnumerator BlinkingShield()
    {
        isInjuring = true;
        color.a = 0.5f;
        newRenderer.material.color = color;
        yield return new WaitForSeconds(injurdDuration);
        color.a = 1f;
        newRenderer.material.color = color;
        isInjuring = false;
    }

    void Movement()
    {

        rb.velocity = new Vector2(Input.GetAxis(HORIZONTAL) * movingSpeed, rb.velocity.y);
        if (JoyStickController.d)
            rb.velocity = new Vector2(1 * movingSpeed, rb.velocity.y);
        if (JoyStickController.a)
            rb.velocity = new Vector2(-1 * movingSpeed, rb.velocity.y);


        //rb.AddForce(new Vector2(Input.GetAxis(HORIZONTAL) * movingSpeed, rb.velocity.y), ForceMode2D.Impulse);

        //if (IsGrounded())
        //{
        jump();
            WalkWithTool();
            turning();
        //}
        
        attacking();       
        //WalkWithCode();



    }

    bool IsGrounded()
    {
        RaycastHit2D raycast = Physics2D.BoxCast(bc.bounds.center, bc.bounds.size, 0f, Vector2.down, .1f, Ground);
        return raycast.collider != null;
    }
    
    void WalkWithTool() {


        

        if (!IsGrounded() && (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A) || JoyStickController.d || JoyStickController.a)) 
        {
            anim.SetBool("isJumping", true);
            isJumping = true;          
        } 
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A) || JoyStickController.d || JoyStickController.a)
        {
            isWalking = true;

        }


        anim.SetBool("isWalking", isWalking);
        //isAttacking = false;
        isWalking = false;


    }

    void jump()
    {
        if (IsGrounded())
        {
            anim.SetBool("isJumping", false);
            isJumping = false;
}
        else 
        {
            anim.SetBool("isJumping", true);
            isJumping = true;
        }

        if (IsGrounded() && (Input.GetKeyDown(KeyCode.K) || JoyStickController.k))
        {
            rb.AddForce(new Vector2(rb.velocity.x, rb.velocity.y + jumpHeight), ForceMode2D.Impulse);
            jumpSound.Play();
        }


        


    }

    void turning()
    {
        if (Input.GetKey(KeyCode.D) || JoyStickController.d)
        {
            rb.transform.localScale = new Vector3(Math.Abs(rb.transform.localScale.x), rb.transform.localScale.y, rb.transform.localScale.z);
        }
        else if (Input.GetKey(KeyCode.A) || JoyStickController.a)
        {
            rb.transform.localScale = new Vector3(-Math.Abs(rb.transform.localScale.x), rb.transform.localScale.y, rb.transform.localScale.z);
        }
    }

    void attacking()
    {
        if (Input.GetKey(KeyCode.J))
        {
            isAttacking = true;
        }

        anim.SetBool("isAttacking", isAttacking);
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerAttackAnimation"))
        {
            isAttacking = false;
        }
    }

    void WalkWithCode() {

        if (Input.GetKey(KeyCode.D))
        {
            rb.transform.localScale = new Vector3(Math.Abs(rb.transform.localScale.x), rb.transform.localScale.y, rb.transform.localScale.z);
            anim.Play("PlayerWalkAnimation");
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rb.transform.localScale = new Vector3(-Math.Abs(rb.transform.localScale.x), rb.transform.localScale.y, rb.transform.localScale.z);
            anim.Play("PlayerWalkAnimation");
        }
        else
        {
            anim.Play("PlayerIdleAnimation");
        }

    }

}
